import { pokemonListService } from '@/service/pokemonListService'
import { store } from '@/store/store'
import { Card } from '@/view/components/card/Card'
import { CardList } from '@/view/components/cardList/CardList'
import { Loader } from '@/view/components/loader/Loader'
import { Pagination } from '@/view/components/pagination/Pagination'
import { observer } from 'mobx-react-lite'
import { useEffect } from 'react'
import styles from './home.module.scss'

export const Home = observer(() => {
	useEffect(() => {
		pokemonListService.getPokemonList()
	}, [store.pageNumber])

	return (
		<main className={styles.home}>
			{store.pokemonListPage ? (
				<>
					<CardList>
						{Object.values(store.pokemonListPage).map(pokemon => {
							return <Card key={pokemon.id} pokemon={pokemon} />
						})}
					</CardList>
					<Pagination
						totalPages={store.totalPages}
						pageNumber={store.pageNumber}
						setPageNumber={store.setPageNumber}
					/>
				</>
			) : (
				<Loader />
			)}
		</main>
	)
})
