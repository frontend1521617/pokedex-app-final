import styles from './notFoundPage.module.scss'

export const NotFoundPage = () => {
	return (
		<main className={styles.errorContainer}>
			<div className={styles.error}>404</div>
		</main>
	)
}
