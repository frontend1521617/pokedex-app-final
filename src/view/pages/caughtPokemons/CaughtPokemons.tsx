import { store } from '@/store/store'
import { Card } from '@/view/components/card/Card'
import { CardList } from '@/view/components/cardList/CardList'
import { Pagination } from '@/view/components/pagination/Pagination'
import { observer } from 'mobx-react-lite'
import styles from './caughtPokemons.module.scss'

export const CaughtPokemons = observer(() => {
	return (
		<main className={styles.caughtPokemons}>
			{store.caughtPokemonsPage ? (
				<>
					<CardList>
						{Object.values(store.caughtPokemonsPage).map(pokemon => {
							return <Card key={pokemon.id} pokemon={pokemon} />
						})}
					</CardList>
					<Pagination
						totalPages={store.caughtPokemonsTotalPages}
						pageNumber={store.caughtPokemonsPageNumber}
						setPageNumber={store.setCaughtPokemonsPageNumber}
					/>
				</>
			) : (
				<div className={styles.emptyList}>No caught pokemons :(</div>
			)}
		</main>
	)
})
