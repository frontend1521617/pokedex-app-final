import { BrowserRouter, Route, Routes } from 'react-router-dom'

import { PokemonDetails } from './components/pokemonDetails/pokemonDetails'
import { CaughtPokemons } from './pages/caughtPokemons/CaughtPokemons'
import { Home } from './pages/home/Home'
import { NotFoundPage } from './pages/notFoundPage/NotFoundPage'
import { Pokedex } from './pages/pokedex/Pokedex'

export const App = () => {
	return (
		<BrowserRouter>
			<Routes>
				<Route path='/' element={<Pokedex />}>
					<Route index element={<Home />} />
					<Route path='home' element={<Home />} />
					<Route path='caught' element={<CaughtPokemons />} />

					<Route path='home/:id' element={<PokemonDetails />} />
					<Route path='caught/:id' element={<PokemonDetails />} />
				</Route>
				<Route path='*' element={<NotFoundPage />}></Route>
			</Routes>
		</BrowserRouter>
	)
}
