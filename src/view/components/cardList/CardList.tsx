import { FC, ReactNode } from 'react'
import styles from './cardList.module.scss'

interface IPokemonListProps {
	children: ReactNode
}

export const CardList: FC<IPokemonListProps> = ({ children }) => {
	return <ul className={styles.cardList}>{children}</ul>
}
