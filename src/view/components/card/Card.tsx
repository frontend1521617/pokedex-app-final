import { IPokemon } from '@/model/model'
import { pokemonListService } from '@/service/pokemonListService'
import { store } from '@/store/store'
import { observer } from 'mobx-react-lite'
import { FC } from 'react'
import { Link } from 'react-router-dom'
import styles from './card.module.scss'

interface ICardProps {
	pokemon: IPokemon
}
export const Card: FC<ICardProps> = observer(({ pokemon }) => {
	const pokemonDetailsHandler = () => {
		store.setCurrentPokemon(pokemon)
	}
	return (
		<li className={styles.card}>
			<Link
				className={styles.pokemonLink}
				to={`/home/${pokemon.id}`}
				onClick={pokemonDetailsHandler}
			>
				id: {pokemon.id} {pokemon.name[0].toUpperCase() + pokemon.name.slice(1)}
			</Link>
			<img className={styles.cardImg} src={pokemon.avatar} alt={pokemon.name} />
			<button
				onClick={() => pokemonListService.catchPokemon(pokemon)}
				{...(pokemon.catch ? { disabled: true } : {})}
				className={styles.btn}
			>
				{pokemon.catch ? 'Caught' : 'Catch me!'}
			</button>
		</li>
	)
})
