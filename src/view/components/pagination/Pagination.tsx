import { FC } from 'react'
import styles from './pagination.module.scss'

interface IPaginationProps {
	totalPages: number
	pageNumber: number
	setPageNumber(pageNumber: number): void
}

export const Pagination: FC<IPaginationProps> = ({
	totalPages,
	pageNumber,
	setPageNumber,
}) => {
	const clickPrevHandler = () => {
		if (pageNumber <= 1) return
		setPageNumber(pageNumber - 1)
	}
	const clickNextHandler = () => {
		if (pageNumber >= totalPages) return
		setPageNumber(pageNumber + 1)
	}
	return (
		<div className={styles.pagination}>
			<button className={styles.btnPrev} onClick={clickPrevHandler}></button>
			<span className={styles.pageNumber}>
				{pageNumber} / {totalPages}
			</span>
			<button className={styles.btnNext} onClick={clickNextHandler}></button>
		</div>
	)
}
