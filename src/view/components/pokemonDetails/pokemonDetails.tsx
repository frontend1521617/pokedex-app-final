import { store } from '@/store/store'
import { observer } from 'mobx-react-lite'
import { useNavigate } from 'react-router-dom'
import styles from './pokemonDetails.module.scss'

export const PokemonDetails = observer(() => {
	const navigate = useNavigate()
	return (
		<main className={styles.pokemonDetails}>
			<div className={styles.pokemonDetailsCard}>
				<div className={styles.pokemonDetailsHeader}>
					<button
						className={styles.btnPokemonDetails}
						onClick={() => {
							navigate(-1)
						}}
					></button>
					<h2>
						id: {store.currentPokemon.id}{' '}
						{store.currentPokemon.name[0].toUpperCase() +
							store.currentPokemon.name.slice(1)}
					</h2>
				</div>

				<img
					className={styles.imgPokemonDetails}
					src={store.currentPokemon.avatar}
					alt={store.currentPokemon.name}
				/>
				<p>Abilities: {store.currentPokemon.abilities.join(', ')}</p>
				<p>
					{store.currentPokemon.catch ? (
						<span>Caught: {store.currentPokemon.dateOfCaught}</span>
					) : (
						<span>No catch</span>
					)}
				</p>
			</div>
		</main>
	)
})
