import { NavLink } from 'react-router-dom'
import styles from './header.module.scss'

export const Header = () => {
	return (
		<header className={styles.header}>
			<img className={styles.logo} src='./logo.png' alt='logo'></img>
			<nav className={styles.headerNav}>
				<NavLink
					to='/home'
					className={props =>
						!props.isActive ? styles.headerLink : styles.headerLinkActive
					}
				>
					Home
				</NavLink>
				<NavLink
					to='/caught'
					className={props =>
						!props.isActive ? styles.headerLink : styles.headerLinkActive
					}
				>
					Caught pokemons
				</NavLink>
			</nav>
		</header>
	)
}
