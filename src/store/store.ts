import { IPages, IPokemon, IPokemonList, IStore } from '@/model/model'
import { makeAutoObservable } from 'mobx'
class Store implements IStore {
	pokemonListPages: IPages = localStorage.pokemonListPages
		? JSON.parse(localStorage.pokemonListPages)
		: {}
	pageNumber: number = 1
	totalPages: number = 1
	caughtPokemonsPages: IPages = localStorage.caughtPokemonsPages
		? JSON.parse(localStorage.caughtPokemonsPages)
		: {}
	caughtPokemonsPageNumber: number = 1
	caughtPokemonsTotalPages: number = localStorage.caughtPokemonsTotalPages
		? localStorage.caughtPokemonsTotalPages
		: 1
	currentPokemon: IPokemon

	constructor() {
		makeAutoObservable(this, {}, { deep: true, autoBind: true })
	}

	get pokemonListPage() {
		return this.pokemonListPages[this.pageNumber]
	}
	pushPokemonListPage(pokemonList: IPokemonList) {
		this.pokemonListPages[this.pageNumber] = pokemonList
	}
	catchPokemonAction(id: number, date: string) {
		this.pokemonListPages[this.pageNumber][id].catch = true
		this.pokemonListPages[this.pageNumber][id].dateOfCaught = date
		localStorage.pokemonListPages = JSON.stringify(this.pokemonListPages)
	}

	setPageNumber(pageNumber: number) {
		this.pageNumber = pageNumber
	}
	setTotalPages(total: number) {
		this.totalPages = total
	}
	get caughtPokemonsPage() {
		return this.caughtPokemonsPages[this.caughtPokemonsPageNumber]
	}
	pushCaughtPokemon(id: number) {
		if (Object.keys(this.caughtPokemonsPages).length) {
			let pagelength = Object.keys(
				this.caughtPokemonsPages[this.caughtPokemonsTotalPages]
			).length
			if (pagelength >= 6) {
				this.caughtPokemonsTotalPages += 1
				this.caughtPokemonsPages[this.caughtPokemonsTotalPages] = {}
			}
		} else {
			this.caughtPokemonsPages[this.caughtPokemonsTotalPages] = {}
		}
		this.caughtPokemonsPages[this.caughtPokemonsTotalPages][id] =
			this.pokemonListPages[this.pageNumber][id]
		localStorage.caughtPokemonsPages = JSON.stringify(this.caughtPokemonsPages)
		localStorage.caughtPokemonsTotalPages = this.caughtPokemonsTotalPages
	}
	setCaughtPokemonsPageNumber(pageNumber: number) {
		this.caughtPokemonsPageNumber = pageNumber
	}
	setCurrentPokemon(pokemon: IPokemon) {
		this.currentPokemon = pokemon
	}
}
export const store = new Store()
