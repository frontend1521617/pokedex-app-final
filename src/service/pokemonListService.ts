import { IPokemon, IPokemonList, IPokemonListService } from '@/model/model'
import { store } from '@/store/store'
import { api } from '@/transport/pokemonAPI'

class PokemonListService implements IPokemonListService {
	limit: number = 6
	async getPokemonList() {
		if (!store.pokemonListPage || store.totalPages === 1) {
			let pokemonList: IPokemonList = {}
			let page: IPokemon[] = await api.fetchPokemonList(
				this.limit,
				(store.pageNumber - 1) * this.limit
			)

			page.forEach((pokemon: IPokemon) => {
				pokemonList[pokemon.id] = pokemon
			})
			store.setTotalPages(api.getTotalCount() / this.limit)
			if (!store.pokemonListPage) store.pushPokemonListPage(pokemonList)
		}
	}
	getDate() {
		const date = new Date()
		const day = date.getDate()
		const month = date.getMonth()
		const year = date.getFullYear()
		const hours = date.getHours()
		const minutes = date.getMinutes()
		const seconds = date.getSeconds()

		const fullDate = `${day < 10 ? `0${day}` : day}.${
			month + 1 < 10 ? `0${month + 1}` : month + 1
		}.${year} ${hours < 10 ? `0${hours}` : hours}:${
			minutes < 10 ? `0${minutes}` : minutes
		}:${seconds < 10 ? `0${seconds}` : seconds}`
		return fullDate
	}
	catchPokemon(pokemon: IPokemon) {
		const date = this.getDate()
		store.catchPokemonAction(pokemon.id, date)
		store.pushCaughtPokemon(pokemon.id)
	}
}

export const pokemonListService = new PokemonListService()
