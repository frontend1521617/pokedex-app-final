export interface IPokemon {
	id: number
	name: string
	avatar: string
	catch: boolean
	dateOfCaught: string
	abilities: string[]
}
export interface IAbility {
	name: string
	url: string
}

export interface IAbilitiesItem {
	ability: IAbility
	is_hidden: boolean
	slot: number
}
export interface IPokemonList {
	[id: number]: IPokemon
}
export interface IPokemonListItem {
	name: string
	url: string
}

export interface IPages {
	[key: number]: IPokemonList
}

export interface IPokemonAPI {
	baseURL: string
	fetchPokemon(pokemonListItem: IPokemonListItem): Promise<IPokemon>
	fetchPokemonList(limit: number, offset: number): Promise<IPokemon[]>
}

export interface IStore {
	pokemonListPages: IPages
	pageNumber: number
	totalPages: number
	caughtPokemonsPages: IPages
	caughtPokemonsPageNumber: number
	caughtPokemonsTotalPages: number

	get pokemonListPage(): IPokemonList
	pushPokemonListPage(pokemonList: IPokemonList): void
}

export interface IPokemonListService {
	getPokemonList(): void
}
