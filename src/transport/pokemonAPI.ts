import {
	IAbilitiesItem,
	IPokemon,
	IPokemonAPI,
	IPokemonListItem,
} from '@/model/model'

class PokemonAPI implements IPokemonAPI {
	baseURL = 'https://pokeapi.co/api/v2/pokemon/'
	totalCount = 1

	async fetchPokemon(item: IPokemonListItem): Promise<IPokemon> {
		let response = await fetch(item.url)
		let pokemon = await response.json()
		let abilities = pokemon.abilities.map((abilitiesItem: IAbilitiesItem) => {
			return abilitiesItem.ability.name
		})
		return {
			id: pokemon.id,
			name: pokemon.name,
			avatar: pokemon.sprites.front_default,
			catch: false,
			dateOfCaught: '',
			abilities: abilities,
		}
	}

	async fetchPokemonList(limit: number, offset: number): Promise<IPokemon[]> {
		let response = await fetch(
			`${this.baseURL}?limit=${limit}&offset=${offset}`
		)
		let page = await response.json()
		this.totalCount = page.count
		let pokemonList: IPokemon[] = await Promise.all(
			page.results.map(this.fetchPokemon)
		)

		return pokemonList
	}

	getTotalCount() {
		return this.totalCount
	}
}

export const api = new PokemonAPI()
